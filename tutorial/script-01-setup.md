# AutoHotkey

## Skip if youve done this already

Just a heads up, if you’ve already downloaded and played around with AHK before you can probably skip this video. 

## Welcome / No Programming Experience

AutoHotkey.

Powerful. Easy to learn.

The ultimate automation scripting language for Windows.

At least thats what the website says.

Welcome to my tutorial series on Auto Hotkey. In my experience everything that's on the tin is true. AutoHotkey is a scripting language that gives a powerful interface to control your computer. It has hundreds of commands and easy ways to activate them with keyboard shortcuts. With AutoHotkey skills you can take repetive computing tasks and make them not only easy, but a joy to perform, knowing you beat your computer.

We’re going to explore the basics of the language starting from the very beginning. 

We’ll look at:

1. 
1. 

In this series we are going to look at 

## Installing AHK

Let's install AutoHotkey. Go to `autohotkey.com` and download the "Current Version". Version 2 has been in development for *years*, and its still not finished so nobody is using it. 

[Later do the installation instruction]

## Installing VS Code

I use Visual Studio Code for my code editor. There's a lot of other options for text editors, so if you are already comfortable with yours then you can already skip to the next video because this is the last thing we'll do in this video. 

`https://code.visualstudio.com/`

I guess follow this part later.

Setup the AHK Extension
